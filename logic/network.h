/*
 * network.h
 *
 *  Created on: 28.09.2021
 *      Author: A.Niggebaum
 */

#ifndef NETWORK_H_
#define NETWORK_H_

#define FOLLOWER_GROUP_DIRECT		0x01
#define FOLLOWER_GROUP_INDIRECT		0x02

/**
 * Structure listing all addresses used in the logic. The last index is the number of addresses. This enum must have
 * the same length (+1) as the total number of expected peripherals. It is used to reference assigned addresses, that
 * the logic can easily call sendMessageToDevice(ENUM_OF_ADDRESS);
 *
 * The indices of this enum are used to access the associated peripherals
 */
enum {
	LOGIC_ADDRESS_SENSOR,/**< LOGIC_ADDRESS_INDEX_SENSOR */
	LOGIC_ADDRESS_DRIVER,/**< LOGIC_ADDRESS_INDEX_DRIVER */
	LOGIC_NUMBER_OF_SUPPORTED_PERIPHERALS    /**< LOGIC_ADDRESS_INDEX_MAX */
};

enum {
	LOGIC_SLOW_TIMER_INDICATOR,
	LOGIC_LIGHT_ENGINE_TIMER,
	LOGIC_DAYLIGHT_HARVESTING_TIMER,
	NUMBER_OF_1S_TIMER
};

/**
 * List of light engines in the system.
 */
enum LightEngineEnum {
	LOGIC_LIGHT_ENGINE_ATTACHED,  /**< LOGIC_LIGHT_ENGINE_ATTACHED */
	LOGIC_LIGHT_ENGINE_PERIPHERAL,/**< LOGIC_LIGHT_ENGINE_PERIPHERAL */
	LOGIC_NUMBER_OF_LIGHT_ENGINES /**< LOGIC_NUMBER_OF_LIGHT_ENGINES */
};

enum PersistentVariablesEnum {
	LOGIC_VAR_MODE,
	LOGIC_VAR_DH_TARGET,
	LOGIC_NOF_PERSISTENT_VARS,
};

#endif /* NETWORK_H_ */
