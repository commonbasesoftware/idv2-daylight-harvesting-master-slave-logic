/*
 * network.c
 *
 *  Created on: 28.09.2021
 *      Author: A.Niggebaum
 */

#include <stdint.h>
#include "ilb.h"
#include "network.h"
#include "logic_interface.h"

//
//	Network definition
//

const uint8_t LOGIC_NUMBER_OF_1S_TIMERS	  		 = NUMBER_OF_1S_TIMER;			//!< number of slow timers in logic

// address INDICES for driver peripheral(s)
const uint8_t sensorAddressIndexList[] = {LOGIC_ADDRESS_SENSOR};

// address INDICES for driver peripheral(s)
const uint8_t driverAddressIndexList[] = {LOGIC_ADDRESS_DRIVER};

// configuration instructions for sensor
const uint8_t sensorInstruction1[] = {0x02, 0x04, 0x01};
const uint8_t sensorInstruction2[] = {0x02, 0x05, 0x64};

// packaged configuration instructions for sensor
const configurationElementTypeDef sensorConfigList[] = {
		{3, &sensorInstruction1[0] },
		{3, &sensorInstruction2[0] }
};

// complete set of configuration instructions for sensor
const configurationSetTypeDef sensorConfigSet = {
		2,
		LOGIC_ADDRESS_SENSOR,
		&sensorConfigList[0]
};

// overall set of configuration instruction
// the only one requiring configuration is the sensor
const logicConfigurationTypeDef logic_configurationInstructionSet = {
		2,
		1,
		&sensorConfigSet
};

// network: sensor peripheral group
const NetworkGroupTypeDef sensorPeripheralClassGroup = {
		ILB_PERIPHERAL_MOTION_DAYLIGHT_CLASS,
		1,
		&sensorAddressIndexList[0]
};

// network: driver peripheral group
const NetworkGroupTypeDef driverPeripheralClassGroup = {
		ILB_FOLLOWER_CLASS,
		1,
		&driverAddressIndexList[0]
};

const NetworkGroupTypeDef* networkGroups[2] = {&sensorPeripheralClassGroup, &driverPeripheralClassGroup};

//
//	Light Engines
//

// List of addresses for light engines. This list is at least one entry long and the first entry is always the attached light engine to the module.
// The other entries contain the index of the address for the associated driver peripheral in the address table
uint8_t lightEngineList[LOGIC_NUMBER_OF_LIGHT_ENGINES] = {LOGIC_LIGHT_ENGINE_ATTACHED_MARKER, LOGIC_ADDRESS_DRIVER};

//
//	Network definition
//
// network: complete list of peripherals in network
const NetworkLayoutTypeDef logic_networkLayout =
{
		LOGIC_NUMBER_OF_SUPPORTED_PERIPHERALS,			// total number of peripherals
		LOGIC_NUMBER_OF_1S_TIMERS,						// total number of 1s timers required by logic
		LOGIC_NUMBER_OF_LIGHT_ENGINES,					// number of light engines
		&lightEngineList[0],							// list of light engines
		2,												// number of groups
		&networkGroups[0]								// list of groups
};

//
//	Persistent variables
//
const uint32_t persistentVarsDefaults[] = {0, 0};

const logicPersistentVariables_t logic_nonVolatileVariables = {
		LOGIC_NOF_PERSISTENT_VARS,
		&persistentVarsDefaults[0],
};

//const NonVolatileMemoryDefaultsTypeDef* logic_nonVolatileVariableDefaults = &listOfNonVolatileVariablesDefaults[0];


