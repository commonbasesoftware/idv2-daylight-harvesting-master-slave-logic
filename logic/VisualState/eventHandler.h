/*
 * eventHandler.h
 *
 *  Created on: 25.08.2021
 *      Author: A.Niggebaum
 */

#ifndef VISUALSTATE_EVENTHANDLER_H_
#define VISUALSTATE_EVENTHANDLER_H_

#include "ILBLogicSystem.h"



/* *** macro definitions *** */

/* User completion codes. */

/** UCC_OK. Returned if the function completed successfully. */
#define UCC_OK                0x00u

/** UCC_QUEUE_EMPTY. Returned on retrieval of an event from an empty queue. */
#define UCC_QUEUE_EMPTY       0xfeu

/** UCC_QUEUE_FULL. Returned on adding of an event to a full queue. */
#define UCC_QUEUE_FULL        0xfdu

/** UCC_MEMORY_ERROR. Returned if memory allocation or reallocation failed. */
#define UCC_MEMORY_ERROR      0xfcu

/** UCC_INVALID_PARAMETER. Returned if an invalid parameter is supplied to a function. */
#define UCC_INVALID_PARAMETER 0xfbu



/* *** type definitions *** */

/** User completion code type. Defines completion codes to be returned by the queue handling
    functions. */
typedef unsigned char UCC_TYPE;

#endif /* VISUALSTATE_EVENTHANDLER_H_ */
