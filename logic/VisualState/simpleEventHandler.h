/*
 * simpleEventHandler.h
 *
 *  Created on: 25.08.2021
 *      Author: A.Niggebaum
 */

#ifndef VISUALSTATE_SIMPLEEVENTHANDLER_H_
#define VISUALSTATE_SIMPLEEVENTHANDLER_H_

/* *** include directives *** */

#include "eventHandler.h"



/* *** function declarations *** */

/** Initialize the event queue. Initializes the internal structure of the
    event queue. */
void SEQ_Initialize (void);



/** Add an event to the event queue. If the queue is full, UCC_QUEUE_FULL is
    returned, otherwise UCC_OK is returned. */
UCC_TYPE SEQ_AddEvent (VSTriggerType event);



/** Retrieve an event from the event queue. If the queue is not empty, UCC_OK
    is returned and pEvent will contain the retrieved event. If the queue is
    empty, UCC_QUEUE_EMPTY is returned and pEvent will contain the value for
    the undefined event. */
UCC_TYPE SEQ_RetrieveEvent (VSTriggerType* pEvent);



/** Clear the queue. All events are removed from the queue. */
void SEQ_Clear (void);



/** Is an event pending. If the queue is not empty, a non-zero value is
    returned, otherwise zero is returned. */
VS_BOOL SEQ_EventPendingP (void);

#endif /* VISUALSTATE_SIMPLEEVENTHANDLER_H_ */
