/*
 * modes.c
 *
 *  Created on: 18.10.2021
 *      Author: A.Niggebaum
 */

#include <stddef.h>
#include "ilb.h"
#include "modes.h"
#include "logic_interface.h"
#include "network.h"
#include "daylightHarvesting/daylightHarvesting.h"

#define DEFAULT_FADE_UP_MS		200
#define MIRED_DEFAULT			0x00

#define NUMBER_OF_LIGHT_ENGINES	4

#define DH_TRIGGER_INTERVAL_S	1

#define SEND_INSTRUCTION_TIMEOUT_MS	500

#define DH_AUTOMATIC_CALIBRATION_FLAG	0

typedef enum ModeTimerState {
	MODE_TIMER_PAUSED,
	MODE_TIMER_RUNNING,
} ModeTimerState_t;

typedef enum DHAlgorithmState {
	DAYLIGHT_HARVESTING_PAUSED,
	DAYLIGHT_HARVESTING_ACTIVE,
} DHState_t;
static DHState_t DHState = DAYLIGHT_HARVESTING_ACTIVE;

//
//	Library of settings
//

//
// Open Office Scenario
LightEngineOutput_t openOfficeDirectList[] = {
		{0xFF, MIRED_DEFAULT, DEFAULT_FADE_UP_MS, 15*60},
		{0x4D, MIRED_DEFAULT, 1*60*1000, 15*16},
		{0x00, MIRED_DEFAULT, 5*1000, LIGHT_ENGINE_HOLD_TIME_FOREVER}
};
LightEngineOutput_t openOfficeIndirectList[] = {
		{0xFF, MIRED_DEFAULT, DEFAULT_FADE_UP_MS, 76*60},
		{0x00, MIRED_DEFAULT, 60*1000, LIGHT_ENGINE_HOLD_TIME_FOREVER}
};
LightEngineOutputSequence_t DirectIndirectOpenOffice[] = {
		{LIGHT_ENGINE_ATTACHED, 0x00, ACTIVATED, 3, 0, &openOfficeDirectList[0]},
		{LIGHT_ENGINE_PERIPHERAL, LOGIC_ADDRESS_DRIVER, DEACTIVATED, 2, 0, &openOfficeIndirectList[0]},
		{LIGHT_ENGINE_FOLLOWER, FOLLOWER_GROUP_DIRECT, ACTIVATED, 3, 0, &openOfficeDirectList[0]},
		{LIGHT_ENGINE_FOLLOWER, FOLLOWER_GROUP_INDIRECT, DEACTIVATED, 2, 0, &openOfficeIndirectList[0]}
};

//
//	Single Office Scenario
LightEngineOutput_t singleOfficeDirectList[] = {
		{0xFF, MIRED_DEFAULT, DEFAULT_FADE_UP_MS, 15*60},
		{0x00, MIRED_DEFAULT, 1*60*1000, LIGHT_ENGINE_HOLD_TIME_FOREVER}
};
LightEngineOutput_t singleOfficeIndirectList[] = {
		{0xFF, MIRED_DEFAULT, DEFAULT_FADE_UP_MS, 15*60},
		{0x4D, MIRED_DEFAULT, 60*1000, 2*60},
		{0x00, MIRED_DEFAULT, 60*1000, LIGHT_ENGINE_HOLD_TIME_FOREVER}
};
LightEngineOutputSequence_t DirectIndirectSingleOffice[] = {
		{LIGHT_ENGINE_ATTACHED, 0x00, ACTIVATED, 2, 0, &singleOfficeDirectList[0]},
		{LIGHT_ENGINE_PERIPHERAL, LOGIC_ADDRESS_DRIVER, DEACTIVATED, 3, 0, &singleOfficeIndirectList[0]},
		{LIGHT_ENGINE_FOLLOWER, FOLLOWER_GROUP_DIRECT, ACTIVATED, 2, 0, &singleOfficeDirectList[0]},
		{LIGHT_ENGINE_FOLLOWER, FOLLOWER_GROUP_INDIRECT, DEACTIVATED, 3, 0, &singleOfficeIndirectList[0]}
};

//
// Corridor Mode
LightEngineOutput_t corridorDirectList[] = {
		{0xFF, MIRED_DEFAULT, DEFAULT_FADE_UP_MS, 5*60},
		{0x20, MIRED_DEFAULT, 60*1000, LIGHT_ENGINE_HOLD_TIME_FOREVER}
};
LightEngineOutput_t corridorIndirectList[] = {
		{0xFF, MIRED_DEFAULT, DEFAULT_FADE_UP_MS, 5*60},
		{0x4D, MIRED_DEFAULT, 60*1000, 2*60},
		{0x00, MIRED_DEFAULT, 5*1000, LIGHT_ENGINE_HOLD_TIME_FOREVER}
};
LightEngineOutputSequence_t DirectIndirectCorridor[] = {
		{LIGHT_ENGINE_ATTACHED, 0x00, ACTIVATED, 2, 0, &corridorDirectList[0]},
		{LIGHT_ENGINE_PERIPHERAL, LOGIC_ADDRESS_DRIVER, DEACTIVATED, 3, 0, &corridorIndirectList[0]},
		{LIGHT_ENGINE_FOLLOWER, FOLLOWER_GROUP_DIRECT, ACTIVATED, 2, 0, &corridorDirectList[0]},
		{LIGHT_ENGINE_FOLLOWER, FOLLOWER_GROUP_INDIRECT, DEACTIVATED, 3, 0, &corridorIndirectList[0]}
};

//
//	Classroom
LightEngineOutput_t classroomList[] = {
		{0xFF, MIRED_DEFAULT, DEFAULT_FADE_UP_MS, LIGHT_ENGINE_HOLD_TIME_FOREVER},
};
LightEngineOutputSequence_t DirectIndirectClassroom[] = {
		{LIGHT_ENGINE_ATTACHED, 0x00, ACTIVATED, 1, 0, &classroomList[0]},
		{LIGHT_ENGINE_PERIPHERAL, LOGIC_ADDRESS_DRIVER, DEACTIVATED, 1, 0, &classroomList[0]},
		{LIGHT_ENGINE_FOLLOWER, FOLLOWER_GROUP_DIRECT, ACTIVATED, 1, 0, &classroomList[0]},
		{LIGHT_ENGINE_FOLLOWER, FOLLOWER_GROUP_INDIRECT, DEACTIVATED, 1, 0, &classroomList[0]}
};

//
//	Always on mode
LightEngineOutputSequence_t DirectIndirectOn[] = {
		{LIGHT_ENGINE_ATTACHED, 0x00, DEACTIVATED, 1, 0, &classroomList[0]},
		{LIGHT_ENGINE_PERIPHERAL, LOGIC_ADDRESS_DRIVER, DEACTIVATED, 1, 0, &classroomList[0]},
		{LIGHT_ENGINE_FOLLOWER, FOLLOWER_GROUP_DIRECT, DEACTIVATED, 1, 0, &classroomList[0]},
		{LIGHT_ENGINE_FOLLOWER, FOLLOWER_GROUP_INDIRECT, DEACTIVATED, 1, 0, &classroomList[0]}
};

//
//	Main structure with all important information
//
const LightEngineMode_t LightEngineModeList[LUMINAIRE_MODE_MAX] = {
		{0xFF, 0, DEACTIVATED, NULL},													// LUMINAIRE_MODE_DEFAULT (empty)
		{0xC0, NUMBER_OF_LIGHT_ENGINES, ACTIVATED, &DirectIndirectOpenOffice[0]},		// LUMINAIRE_MODE_OPEN_OFFICE
		{0x3C, NUMBER_OF_LIGHT_ENGINES, ACTIVATED, &DirectIndirectSingleOffice[0]},		// LUMINAIRE_MODE_SINGLE_OFFICE
		{0x03, NUMBER_OF_LIGHT_ENGINES, ACTIVATED, &DirectIndirectCorridor[0]},			// LUMINAIRE_MODE_CORRIDOR
		{0xFC, NUMBER_OF_LIGHT_ENGINES, DEACTIVATED, &DirectIndirectClassroom[0]},		// LUMINAIRE_MODE_CLASSROOM
		{0x3F, NUMBER_OF_LIGHT_ENGINES, DEACTIVATED, &DirectIndirectOn[0]},				// LUMINAIRE_MODE_ON
};

//
//	Local variables
//
static LuminaireModes_t currentMode = LUMINAIRE_MODE_DEFAULT;
static uint32_t timerCounter[NUMBER_OF_LIGHT_ENGINES] = {0};
static ModeTimerState_t modeTimerStatus = MODE_TIMER_PAUSED;

//
//	Function prototypes
//
void lightEngineOutputHandle(void);
void applyLightEngineOutput(LightEngineOutputSequence_t* def);
void daylightHarvestingHandle(void);
void daylightHarvestingCallback(DaylightHarvestingChangeRequestEnumTypeDef change, uint8_t directLevel, uint8_t indirectLevel, uint16_t transitionTimeMS);

//
//	Function implementations
//
/**
 * Initialise the modes
 */
void modes_init(void)
{
	if (currentMode == LUMINAIRE_MODE_DEFAULT)
	{
		currentMode++;
	}
	modeTimerStatus = MODE_TIMER_RUNNING;
	coreLib_startTimer(LOGIC_LIGHT_ENGINE_TIMER, lightEngineOutputHandle, 1);

	// start timer for daylight harvesting
	coreLib_startTimer(LOGIC_DAYLIGHT_HARVESTING_TIMER, daylightHarvestingHandle, DH_TRIGGER_INTERVAL_S);
}

/**
 * Cycle through the modes
 *
 * @param colorDef
 */
void modes_nextMode(uint8_t* colorDef, uint8_t* motionActive)
{
	currentMode++;
	if (currentMode >= LUMINAIRE_MODE_MAX)
	{
		currentMode = LUMINAIRE_MODE_OPEN_OFFICE;
	}
	// push color definition to variable
	*colorDef = LightEngineModeList[currentMode].colorDef;
	*motionActive = LightEngineModeList[currentMode].motionSensorActive;

	// apply current setting
	modes_resetCurrentMode();

	// change persistent variable
	uint32_t tmp = currentMode;
	persistentVariables_set(LOGIC_VAR_MODE, tmp);
	persistentVariables_writeToMemory();

	// reset daylight harvesting algorithm
	DHAlgorithm_Reset();
}

/**
 * 	Reset the current mode to its initial state
 */
void modes_resetCurrentMode(void)
{
	// this is also the indicator that we need to start the timer again
	modeTimerStatus = MODE_TIMER_RUNNING;
	// reset timers and apply settings. Settings are only applied if we are not in the start output (saves messages)
	for (uint8_t i=0; i<NUMBER_OF_LIGHT_ENGINES; i++)
	{
		// check if we need to reset the state
		if (LightEngineModeList[currentMode].outputSequences[i].currentOutput != 0)
		{
			// reset output state handler
			LightEngineModeList[currentMode].outputSequences[i].currentOutput = 0;
			// apply output
			applyLightEngineOutput(&LightEngineModeList[currentMode].outputSequences[i]);
		}
		// reset timer
		timerCounter[i] = LightEngineModeList[currentMode].outputSequences[i].listOfOutputs[0].holdTimeS;

	}
}

/**
 * Reset the running timers
 */
void modes_stopTimer(void)
{
	modeTimerStatus = MODE_TIMER_PAUSED;
}

/**
 * Loads settings from memory and fills in the color definition
 *
 * @param colorDef
 */
void modes_loadFromMemory(uint8_t* colorDef, uint8_t* motionActive, uint8_t* automaticDHCalibrationActive)
{
	// recover mode
	uint32_t tmp = 0;
	persistentVariables_get(LOGIC_VAR_MODE, &tmp);
	currentMode = (tmp & 0xFF);
	// sanity check
	if (currentMode >= LUMINAIRE_MODE_MAX)
	{
		currentMode = LUMINAIRE_MODE_DEFAULT;
	}
	// recover target value
	persistentVariables_get(LOGIC_VAR_DH_TARGET, &tmp);
	if (tmp == DH_AUTOMATIC_CALIBRATION_FLAG)
	{
	*automaticDHCalibrationActive = 1;
	} else {
		DHAlgorithm_SetTargetLevel(tmp & 0xFFFF);
		*automaticDHCalibrationActive = 0;
	}
	modes_init();
	*colorDef = LightEngineModeList[currentMode].colorDef;
	*motionActive = LightEngineModeList[currentMode].motionSensorActive;
}

/**
 * Apply the provided output to the light engines of the current mode that are specified as targets for daylight control
 * @param level
 * @param mired
 * @param fadeTimeMS
 */
void modes_setOutputOfDaylightTargets(uint8_t level, uint16_t mired, uint16_t fadeTimeMS)
{
	// iterate over primary setting and apply output to light engines that are specified as targets
	for (uint8_t i=0; i<LightEngineModeList[currentMode].nOfLightEngines; i++)
	{
		if (LightEngineModeList[currentMode].outputSequences[i].daylightHarvestingTarget == ACTIVATED)
		{
			// store value to go back to when we re-engage daylight harvesting
			LightEngineModeList[currentMode].outputSequences[i].listOfOutputs[0].level = level;
			switch (LightEngineModeList[currentMode].outputSequences[i].type)
			{
			case LIGHT_ENGINE_ATTACHED:
				coreLib_setLightEngine(LightEngineModeList[currentMode].outputSequences[i].addressOrGroup, level, mired, fadeTimeMS, INTENSITY_LOOKUP_LINEAR);
				break;

			case LIGHT_ENGINE_PERIPHERAL:
				coreLib_setLightEngine(LightEngineModeList[currentMode].outputSequences[i].addressOrGroup, level, mired, fadeTimeMS, INTENSITY_LOOKUP_LINEAR);
				break;

			case LIGHT_ENGINE_FOLLOWER:
				coreLib_setFollower(LightEngineModeList[currentMode].outputSequences[i].addressOrGroup, level, mired, fadeTimeMS);
				break;
			}
		}
	}
}


//
//	Private helper functions
//

/**
 * Handle the timers managing the modes
 */
void lightEngineOutputHandle(void)
{
	if (modeTimerStatus == MODE_TIMER_PAUSED)
	{
		coreLib_startTimer(LOGIC_LIGHT_ENGINE_TIMER, lightEngineOutputHandle, 1);
		return;
	}
	// count down
	for (uint8_t i=0; i<NUMBER_OF_LIGHT_ENGINES; i++)
	{
		if (timerCounter[i] != LIGHT_ENGINE_HOLD_TIME_FOREVER &&
			timerCounter[i] > 0)
		{
			// count down
			timerCounter[i]--;
			// check if we need to advance a level
			if (timerCounter[i] == 0)
			{
				// check if we can advance a step
				if (LightEngineModeList[currentMode].outputSequences[i].currentOutput < LightEngineModeList[currentMode].outputSequences[i].nTotalOutputs)
				{
					// advance
					LightEngineModeList[currentMode].outputSequences[i].currentOutput++;
					// apply output
					applyLightEngineOutput(&LightEngineModeList[currentMode].outputSequences[i]);
					// set timer
					timerCounter[i] = LightEngineModeList[currentMode].outputSequences[i].listOfOutputs[LightEngineModeList[currentMode].outputSequences[i].currentOutput].holdTimeS;
				}
			}
		}
	}
	// re schedule
	coreLib_startTimer(LOGIC_LIGHT_ENGINE_TIMER, lightEngineOutputHandle, 1);
}

/**
 * Apply the output to a light engine
 * @param def
 */
void applyLightEngineOutput(LightEngineOutputSequence_t* def)
{
	switch (def->type)
	{
	case LIGHT_ENGINE_ATTACHED:
		coreLib_setLightEngine(def->addressOrGroup, def->listOfOutputs[def->currentOutput].level, def->listOfOutputs[def->currentOutput].mired, def->listOfOutputs[def->currentOutput].fadeTimeMS, INTENSITY_LOOKUP_LINEAR);
		break;

	case LIGHT_ENGINE_PERIPHERAL:
		coreLib_setLightEngine(def->addressOrGroup, def->listOfOutputs[def->currentOutput].level, def->listOfOutputs[def->currentOutput].mired, def->listOfOutputs[def->currentOutput].fadeTimeMS, INTENSITY_LOOKUP_LINEAR);
		break;

	case LIGHT_ENGINE_FOLLOWER:
		coreLib_setFollower(def->addressOrGroup, def->listOfOutputs[def->currentOutput].level, def->listOfOutputs[def->currentOutput].mired, def->listOfOutputs[def->currentOutput].fadeTimeMS);
		break;
	}
}


//
//	Daylight harvesting
//
void modes_setDHAlgorithmState(uint8_t state)
{
	if (state == 0)
	{
		DHState = DAYLIGHT_HARVESTING_PAUSED;
	} else {
		DHState = DAYLIGHT_HARVESTING_ACTIVE;
	}
}

/**
 * Deal with the daylight harvesting algorithm
 */
void daylightHarvestingHandle(void)
{
	// first, check if we are paused or not (overwrite via logic)
	if (DHState == DAYLIGHT_HARVESTING_ACTIVE)
	{
		uint8_t DHActive = 0;
		// Check if we are active in the current mode
		for (uint8_t i=0; i<LightEngineModeList[currentMode].nOfLightEngines; i++)
		{
			if (LightEngineModeList[currentMode].outputSequences[i].daylightHarvestingTarget == ACTIVATED &&
				LightEngineModeList[currentMode].outputSequences[i].currentOutput == 0)
			{
				DHActive = 1;
				break;
			}
		}

		if (DHActive == 1)
		{
			// Trigger the algorithm
			DHAlgorithm_Trigger(coreLib_getAddressForIndex(LOGIC_ADDRESS_SENSOR), daylightHarvestingCallback);
		}
	}

	// re-trigger handle
	coreLib_startTimer(LOGIC_DAYLIGHT_HARVESTING_TIMER, daylightHarvestingHandle, DH_TRIGGER_INTERVAL_S);
}

/**
 * Handle requests from the daylight harvesting algorithm
 *
 * @param change
 * @param directLevel
 * @param indirectLevel
 * @param transitionTimeMS
 */
void daylightHarvestingCallback(DaylightHarvestingChangeRequestEnumTypeDef change, uint8_t directLevel, uint8_t indirectLevel, uint16_t transitionTimeMS)
{
	// check if we need do do anything
	if (change == DAYLIGHT_HARVESTING_ALGORITHM_NO_CHANGE)
	{
		return;
	}

	// we need to adjust the output. Set the level
	modes_setOutputOfDaylightTargets(directLevel, 0, transitionTimeMS);
}

void modes_setAutomaticDHCalibration(uint8_t newState)
{
	// mode is disabled
	if (newState == 0)
	{
		// store the value
		persistentVariables_set(LOGIC_VAR_DH_TARGET, DHAlgorithm_GetTargetLevel());
	}
	// mode is enabled
	{
		// store the value
		persistentVariables_set(LOGIC_VAR_DH_TARGET, DH_AUTOMATIC_CALIBRATION_FLAG);
	}
	// write to memory
	persistentVariables_writeToMemory();
}
