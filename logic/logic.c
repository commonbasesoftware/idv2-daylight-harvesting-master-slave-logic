/*
 * logic.c
 *
 *  Created on: 28.09.2021
 *      Author: A.Niggebaum
 */

#include <stdint.h>
#include "ilb.h"
#include "network.h"
#include "logic_interface.h"
#include "modes.h"
#include "daylightHarvesting/daylightHarvesting.h"

// visual state
#include "VisualState/ILBLogicSystem.h"
#include "VisualState/simpleEventHandler.h"

//
//	Private variables
//
static VSTriggerType eventNo = VSStartEvent;		// next event to be handled by visual state system

/**
 * Start routine for logic
 */
void logic_start(void)
{
	modes_init();

	// TODO: temporary
	vsextVar_ILBLogicSystem._Mode_AutomaticDaylightHarvesting = 1;

	VSInitAll();
	SEQ_Initialize();
	SEQ_AddEvent(VSStartEvent);
}

/**
 * Main event loop for logic
 * @return
 */
uint8_t logic_eventLoop(void)
{
	// Handle mode change and settings via Visual State UML
	if (SEQ_EventPendingP())
	{
		SEQ_RetrieveEvent(&eventNo);
		VSDeduct(eventNo);
	}
	return 0;
}

/**
 * Command handler
 * @param m
 */
void logic_handleCommand(ILBMessageStruct* m, uint8_t addressIndex)
{
	// decide what to do, depending on peripheral
	if (addressIndex == LOGIC_ADDRESS_SENSOR)
	{
		// Motion daylight event
		if (m->payload[0] == ILB_REPORT_MOTION_DAYLIGHT_EVENT)
		{
			// reply with OK
			coreLib_respondWithCode(m, ILB_OK);
			// reset current mode
			modes_resetCurrentMode();
			// emit event to logic
			SEQ_AddEvent(MotionDetected);
			return;
		}
		// button event
		else if (m->payload[0] == ILB_REPORT_BUTTON_EVENT &&
				 m->payloadSize == 2)
		{
			// store value in external variable interface structure
			vsextVar_ILBLogicSystem._ButtonHoldTimeMS = (uint16_t)(m->payload[1] * 100);
			// trigger event
			SEQ_AddEvent(ButtonReleased);
			// acknowledge message
			coreLib_respondWithCode(m, ILB_OK);
			return;
		}
		// DEFAULT: message is invalid
		else
		{
			coreLib_respondWithCode(m, ILB_ERROR_NOT_KNOWN);
		}

		// return error
		coreLib_respondWithCode(m, ILB_ERROR_NOT_KNOWN);
	}
	// message from driver peripheral
	else if (addressIndex == LOGIC_ADDRESS_DRIVER)
	{
		// FADE_FINISHED is handled in the IBL library. We don't need to do anyhting
		// SEQ_AddEvent(FadeFinished);
	}
}

/**
 * Required function by base library
 */
void logic_attachedLightEngineFadeFinished(void)
{

}


//
//	Implementation of functions required by visual state logic
//

/**
 * Configure the algorithm
 *
 * @param minimalAllowedLevel
 * @param maximalAllowedLevel
 */
void DH_Configure (VS_UINT8 minimalAllowedLevel, VS_UINT8 maximalAllowedLevel)
{
	DHAlgorithm_Configure(maximalAllowedLevel, minimalAllowedLevel);
}

/**
 * Shortcut for manual setting of daylight level.
 *
 * This routine is used by the manual calibration function. Hence, we don't use any fade time.
 *
 * @param level
 */
void DH_LightEngineSetLevel (VS_UINT8 level, VS_UINT16 fadeTimeMS)
{
	modes_setOutputOfDaylightTargets(level, 0, fadeTimeMS);
}

/**
 * Enables or disables the daylight harvesting algorithm
 *
 * @param newState
 */
void DH_SetActive (VS_BOOL newState)
{
	modes_setDHAlgorithmState(newState);
}

/**
 * Enable or disable automatic calibration upon start of the device
 *
 * @param newState
 */
void DH_SetAutomaticCalibration (VS_BOOL newState)
{
	// set running variables
	if (newState == 0)
	{
		vsextVar_ILBLogicSystem._Mode_AutomaticDaylightHarvesting = 0;
	} else {
		vsextVar_ILBLogicSystem._Mode_AutomaticDaylightHarvesting = 1;
	}
	// store the flag
	modes_setAutomaticDHCalibration(newState);
}

void LevelMeasurementComplete(void)
{
	SEQ_AddEvent(DaylightHarvesting_CalibrationComplete);
}

/**
 * Function triggered when a manual level is required.
 * @param level
 */
void DH_UseAsManualReferenceLevel (VS_UINT8 level)
{
	DHAlgorithm_UseCurrentLevelAsTaget(coreLib_getAddressForIndex(LOGIC_ADDRESS_SENSOR), LevelMeasurementComplete);
}

void DH_UseAsMaxReferenceLevel (void)
{
	DHAlgorithm_MeasureMaximal(coreLib_getAddressForIndex(LOGIC_ADDRESS_SENSOR), LevelMeasurementComplete);
}

void DH_UseAsMinReferenceLevel (void)
{
	DHAlgorithm_MeasureMinimal(coreLib_getAddressForIndex(LOGIC_ADDRESS_SENSOR), LevelMeasurementComplete);
}

/**
 * Load the settings
 *
 * Loads
 * - mode
 * - daylight harvesting
 */
void Settings_load (void)
{
	uint8_t motion = 0;
	uint8_t DHAutomatic = 0;
	modes_loadFromMemory(&vsextVar_ILBLogicSystem._Mode_ColorDef, &motion, &DHAutomatic);
	if (motion == 1)
	{
		vsextVar_ILBLogicSystem._Mode_MotionDetectionActive = 1;
	} else {
		vsextVar_ILBLogicSystem._Mode_MotionDetectionActive = 0;
	}
	if (DHAutomatic == 0)
	{
		vsextVar_ILBLogicSystem._Mode_AutomaticDaylightHarvesting = 0;
	} else {
		vsextVar_ILBLogicSystem._Mode_AutomaticDaylightHarvesting = 1;
	}
}

void ChangeMode (void)
{
	// go to next mode and push color to system
	uint8_t motion = 0;
	modes_nextMode(&vsextVar_ILBLogicSystem._Mode_ColorDef, &motion);
	if (motion)
	{
		vsextVar_ILBLogicSystem._Mode_MotionDetectionActive = 1;
	} else {
		vsextVar_ILBLogicSystem._Mode_MotionDetectionActive = 0;
	}
}

void IndicatorTimerCallback(void)
{
	SEQ_AddEvent(IndicatorTimer_expired);
}

void IndicatorTimer_start (VS_UINT16 durationS)
{
	coreLib_startTimer(LOGIC_SLOW_TIMER_INDICATOR, IndicatorTimerCallback, durationS);
}

void Indicator_Set (VS_UINT8 colorDef, VS_UINT8 patternDef, VS_UINT8 repeatCounter)
{
	// Prepare the message
	ILBMessageStruct instruction;
	instruction.address = coreLib_getAddressForIndex(LOGIC_ADDRESS_SENSOR);
	instruction.identifier = ILB_GENERATE_IDENTIFIER;
	instruction.messageType = ILB_MESSAGE_WRITE;
	instruction.payloadSize = 6;
	instruction.payload[0] = 0x04;
	instruction.payload[1] = 0x00;
	instruction.payload[2] = 0x02;
	instruction.payload[3] = colorDef;
	instruction.payload[4] = patternDef;
	instruction.payload[5] = repeatCounter;

	coreLib_sendMessage(&instruction, NULL, 0, NULL);
}

/**
 * Start breathing the indicator in a set color for a set duration
 * @param colorDef
 * @param repeatCounter
 */
void Indicator_StartBreathe (VS_UINT8 colorDef, VS_UINT8 repeatCounter)
{
	// Prepare the message
	ILBMessageStruct instruction;
	instruction.address = coreLib_getAddressForIndex(LOGIC_ADDRESS_SENSOR);
	instruction.identifier = ILB_GENERATE_IDENTIFIER;
	instruction.messageType = ILB_MESSAGE_WRITE;
	instruction.payloadSize = 6;
	instruction.payload[0] = 0x04;
	instruction.payload[1] = 0x00;
	instruction.payload[2] = 0x01;
	instruction.payload[3] = colorDef;
	instruction.payload[4] = 0x00;
	instruction.payload[5] = repeatCounter;

	coreLib_sendMessage(&instruction, NULL, 0, NULL);
}

/**
 * Start a colourfull blinking pattern of the indicator
 * @param durationS
 */
void Indicator_BlinkRainbow (VS_UINT16 durationS)
{
	if (durationS < 2)
	{
		durationS = 2;
	}
	ILBMessageStruct instruction;
	instruction.address = coreLib_getAddressForIndex(LOGIC_ADDRESS_SENSOR);
	instruction.identifier = ILB_GENERATE_IDENTIFIER;
	instruction.messageType = ILB_MESSAGE_WRITE;
	instruction.payloadSize = 12;
	instruction.payload[0] = 0x04;
	instruction.payload[1] = 0x00;
	instruction.payload[2] = 0x04;
	instruction.payload[3] = 0x03;
	instruction.payload[4] = 0xAA;
	instruction.payload[5] = durationS/2;
	instruction.payload[6] = 0xC0;
	instruction.payload[7] = 0xFC;
	instruction.payload[8] = 0x3C;
	instruction.payload[9] = 0x3F;
	instruction.payload[10] = 0x03;
	instruction.payload[11] = 0x00;

	coreLib_sendMessage(&instruction, NULL, 0, NULL);
}

/**
 * Apply the current mode (again). Resets the current mode
 */
void Mode_ApplyCurrentMode (void)
{
	// start the current mode (again)
	modes_resetCurrentMode();
}
