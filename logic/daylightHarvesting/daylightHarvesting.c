/*
 * daylightHarvesting.c
 *
 *  Created on: 12.07.2021
 *      Author: A.Niggebaum
 *
 *
 */

#include "ilb.h"
#include "daylightHarvesting.h"
#include "logic_interface.h"
#include <string.h>

//
//	private defines and types
//
#define DH_SENSOR_LEVEL_REQUEST_TIMEOUT_MS		500		// timeout allowed for grabbing data from sensor
//
//	Paramters for algorithm
//
#define DH_SENSOR_TARGET_TOLERANCE_PM			10		// counts of daylight sensor readings that are allowed above or below target
#define DH_SENSOR_DARK_COUNTS					0
// natural offset of sensor. Will be subtracted from all measurements
#define DH_ALGORITHM_NUMBER_OF_BANDS			10
#define DH_ALGORITHM_MIN_OUTPUT_DEFAULT			50
#define DH_ALGORITHM_MAX_OUTPUT_DEFAULT			0xFF
#define DH_ALGORITHM_DIMM_DOWN_CNT_THRS			30		// number of consecutive measurements above threshold (too bright) before we start dimming down
#define DH_ALGORITHM_DIMM_UP_CNT_THRS			5		// number of consecutive measurements below threshold (too dimm) before we start dimming up
#define DH_ALGORITHM_DIMM_DOWN_TIME_S			10		// time in ms we dimm down between levels
#define DH_ALGORITHM_DIMM_UP_TIME_S				1		// time in ms we need to dimm up one level

// TEMPORARY: should link to peripheral
#define PERIPHERAL_MOTION_DAYLIGHT_VOLATILE_REGISTER				0x03
#define PERIPHERAL_MOTION_DAYLIGHT_VOLATILE_DAYLIGHT_DIRECT_LSB		0x01

typedef enum DaylightAlgorithmStateEnum
{
	ALGORITHM_STATE_IDLE,			// indicates that the algorithm is processing the inputs
	ALGORITHM_STATE_MANUAL,			// indicates that the algorithm should take a manual measuremetn
	ALGORITHM_STATE_DIMMING_DOWN,	// indicates that the light engines are dimming down and that this must be considered
	ALGORITHM_STATE_DIMMING_UP,		// indicates that the light engines are dimming up and that this must be considered
	ALGORITHM_STATE_ERROR,			// indicates that the algorithm is in a general error state
	ALGORITHM_STATE_DELTA_TOO_SMALL	// indicates that the encountered delta is too small to operate reliably
} DaylightAlgorithmStateEnumTypeDef;

typedef struct DaylightAlgorithmParameterStruct
{
	uint16_t minLevelMeasured;					// minimal level measured
	uint16_t maxLevelMeasured;					// maximal level measured
	uint16_t availableDelta;					// available delta between 0% and 100% lightoutput
	uint16_t targetLevel;						// the target level to stabilise to
	DaylightAlgorithmStateEnumTypeDef state;	// state of the algorithm state machine
	uint8_t consecutiveReadingsAbove;			// number of readings that qualify as above target e.g. too much light output
	uint8_t consecutiveReadingsBelow;			// number of readings that qualify as below target e.g. not enough light output
	uint8_t currentRequestedOutput;				// current output value requested to logic
} DaylightAlgorithmParameterStructTypeDef;

//
//	Function prototypes
//
void sensorReadingReceivedCallback(ILBMessageStruct* m);
void sensorReadingTimedOutCallback(void);
void sensorCommunicationErrorCallback(void);
void updateAlgorithmParameters(void);							// to be called if e.g. min/max of possible range has changed
void algorithmDimmingFinishedCallback(void);					// to be called when the dimming time has finished
uint16_t interpretSensorReading(uint16_t registerContent);			// parses the response of the sensor into a lux value

//
//	Private variables
//
static uint8_t SETTING_minimalAllowedLevel = DH_ALGORITHM_MIN_OUTPUT_DEFAULT;
static uint8_t SETTING_maximalAllowedLevel = DH_ALGORITHM_MAX_OUTPUT_DEFAULT;

static void (*DH_ReadyCallback)(DaylightHarvestingChangeRequestEnumTypeDef change, uint8_t directLevel, uint8_t indirectLevel, uint16_t transitionTimeMS) = NULL;	// holds the pointer to the callback used to signal the logic that theresult is ready
static void (*MeasurementDoneCallback)(void) = NULL;
//static uint16_t daylightTargetLevel = 0;												// target level from sensor

static DaylightAlgorithmParameterStructTypeDef algorithmParameters = {0, 0, 0, 0, ALGORITHM_STATE_IDLE, 0, 0, 0xFF};
static void (*DH_measurementCompleteCallback)(void);									// callback to be called when measurement part of min/max determination is complete

static uint16_t dimmingCount = 0;						//!< counter to track for how long we are dimming up or down

//
//	Function implementation
//

/**
 * Configure the boundaries in which the algorithm can move
 *
 * @param maxLevel
 * @param minLevel
 */
void DHAlgorithm_Configure(uint8_t maxLevel, uint8_t minLevel)
{
	if (maxLevel > minLevel)
	{
		SETTING_minimalAllowedLevel = minLevel;
		SETTING_maximalAllowedLevel = maxLevel;
	} else {
		SETTING_minimalAllowedLevel = maxLevel;
		SETTING_maximalAllowedLevel = minLevel;
	}
}

/**
 * Reset state if e.g. a mode was changed and output level of the light engines may be out of sync
 */
void DHAlgorithm_Reset(void)
{
	algorithmParameters.consecutiveReadingsAbove = 0;
	algorithmParameters.consecutiveReadingsBelow = 0;
	algorithmParameters.currentRequestedOutput = SETTING_maximalAllowedLevel;
	algorithmParameters.state = ALGORITHM_STATE_IDLE;
}

/**
 * Manually set a target level
 * @param level
 */
void DHAlgorithm_SetTargetLevel(uint16_t level)
{
	algorithmParameters.targetLevel = level;
}

/**
 * Accessor for target level
 * @return
 */
uint16_t DHAlgorithm_GetTargetLevel(void)
{
	return algorithmParameters.targetLevel;
}

/**
 * Trigger routine.
 *
 * This routine should be called in regular intervals. Ideally 1s.
 * It requests the reading from the sensor. All further steps are handled when the answer is received.
 *
 * @param sensorAddress Address of sensor to read from
 * @param callback Pointer to function that should be called once the result of the algorithm step is ready
 */
void DHAlgorithm_Trigger(uint8_t sensorAddress, void (*callback)(DaylightHarvestingChangeRequestEnumTypeDef change, uint8_t directLevel, uint8_t indirectLevel, uint16_t transitionTimeMS))
{
	// store callback
	DH_ReadyCallback = callback;

	// 1. request value from sensor
	uint8_t payload[] = {0x03, 0x01, 0x02};	// request filtered daylight level from sensor
	ILBMessageStruct messageToSend;
	messageToSend.address = sensorAddress;
	messageToSend.messageType = ILB_MESSAGE_READ;
	messageToSend.payloadSize = 3;
	memcpy(&messageToSend.payload[0], &payload[0], 3);
	coreLib_sendMessage(&messageToSend, &sensorReadingReceivedCallback, DH_SENSOR_LEVEL_REQUEST_TIMEOUT_MS, &sensorReadingTimedOutCallback);
}

/**
 * Callback when daylight reading from sensor is received.
 *
 * While the trigger routine merely triggers the reading from the sensor, this routine actually performs the step in the algorithm
 */
void sensorReadingReceivedCallback(ILBMessageStruct* m)
{
	// 2. check
	// 2.1. Check message from correct payload
	if (m->payloadSize != 2)
	{
		sensorCommunicationErrorCallback();
		return;
	}
	// 2.2. Extract read value
	uint16_t reading = interpretSensorReading((m->payload[0] << 8) | m->payload[1]);

	// 3. process
	switch (algorithmParameters.state)
	{
	case ALGORITHM_STATE_IDLE:
		// process the value

		// check if we are too bright
		if (reading > DH_SENSOR_TARGET_TOLERANCE_PM + algorithmParameters.targetLevel + algorithmParameters.availableDelta/DH_ALGORITHM_NUMBER_OF_BANDS)
		{
			// we are above threshold. Count up
			algorithmParameters.consecutiveReadingsAbove++;
			algorithmParameters.consecutiveReadingsBelow = 0;
			if (algorithmParameters.consecutiveReadingsAbove >= DH_ALGORITHM_DIMM_DOWN_CNT_THRS)
			{
				// reset
				algorithmParameters.consecutiveReadingsAbove = 0;
				// we are too bright and need to dimm down

				// before we do anyhting, check if we are already at the minimum
				if (algorithmParameters.currentRequestedOutput <= SETTING_minimalAllowedLevel)
				{
					// we are already at the minimu. Quit with no change request
					(*DH_ReadyCallback)(DAYLIGHT_HARVESTING_ALGORITHM_NO_CHANGE, 0, 0, 0);
					return;
				}

				// we still can change. Calcualte new level
				uint8_t newTarget = algorithmParameters.currentRequestedOutput - (SETTING_maximalAllowedLevel - SETTING_minimalAllowedLevel) / DH_ALGORITHM_NUMBER_OF_BANDS;
				if (newTarget < SETTING_minimalAllowedLevel)
				{
					newTarget = SETTING_minimalAllowedLevel;
				}
				// send request to logic
				(*DH_ReadyCallback)(DAYLIGHT_HARVESTING_ALGORITHM_CHANGE_REQUIRED, newTarget, newTarget, DH_ALGORITHM_DIMM_DOWN_TIME_S * 1000);
				// set state of algorithm
				algorithmParameters.state = ALGORITHM_STATE_DIMMING_DOWN;
				algorithmParameters.currentRequestedOutput = newTarget;
				// start the countdown
				dimmingCount = DH_ALGORITHM_DIMM_DOWN_TIME_S + 1;
				return;
			}
		}

		// check if we are too dimm
		if (reading < algorithmParameters.targetLevel - algorithmParameters.availableDelta/DH_ALGORITHM_NUMBER_OF_BANDS - DH_SENSOR_TARGET_TOLERANCE_PM)
		{
			algorithmParameters.consecutiveReadingsBelow++;
			algorithmParameters.consecutiveReadingsAbove = 0;
			if (algorithmParameters.consecutiveReadingsBelow >= DH_ALGORITHM_DIMM_UP_CNT_THRS)
			{
				// reset
				algorithmParameters.consecutiveReadingsBelow = 0;
				// we are too dimm and need to increase output

				// check if we are already at the maximum
				if (algorithmParameters.currentRequestedOutput >= SETTING_maximalAllowedLevel)
				{
					// we are at the maximum. Nothing we can do
					(*DH_ReadyCallback)(DAYLIGHT_HARVESTING_ALGORITHM_NO_CHANGE, 0, 0, 0);
					return;
				}

				// we can dimm up. Calculate step and dimm up
				uint8_t newTarget = algorithmParameters.currentRequestedOutput + (SETTING_maximalAllowedLevel - SETTING_minimalAllowedLevel) / DH_ALGORITHM_NUMBER_OF_BANDS;
				// check for roll over
				if (newTarget < algorithmParameters.currentRequestedOutput)
				{
					newTarget = SETTING_maximalAllowedLevel;
				}
				// send request to logic
				(*DH_ReadyCallback)(DAYLIGHT_HARVESTING_ALGORITHM_CHANGE_REQUIRED, newTarget, newTarget, DH_ALGORITHM_DIMM_UP_TIME_S * 1000);
				// set state of algorithm
				algorithmParameters.state = ALGORITHM_STATE_DIMMING_UP;
				algorithmParameters.currentRequestedOutput = newTarget;
				// start timer
				dimmingCount = DH_ALGORITHM_DIMM_UP_TIME_S + 1;
				return;
			}
		}
		break;

	case ALGORITHM_STATE_MANUAL:
		// take reading as reference
		DHAlgorithm_SetTargetLevel(interpretSensorReading(reading));
		algorithmParameters.state = ALGORITHM_STATE_IDLE;
		if (MeasurementDoneCallback != NULL)
		{
			MeasurementDoneCallback();
		}
		break;

	case ALGORITHM_STATE_DIMMING_DOWN:
		// we are dimming down.
		if (dimmingCount > 0)
		{
			dimmingCount--;
		} else {
			algorithmParameters.state = ALGORITHM_STATE_IDLE;
		}
		// Handle if by any chance it has become to dimm
		if (reading < algorithmParameters.targetLevel - DH_SENSOR_TARGET_TOLERANCE_PM)
		{
			algorithmParameters.consecutiveReadingsBelow++;
			algorithmParameters.consecutiveReadingsAbove = 0;
			if (algorithmParameters.consecutiveReadingsBelow >= DH_ALGORITHM_DIMM_UP_CNT_THRS)
			{
				// break this state and dimm up if necessary
				algorithmParameters.state = ALGORITHM_STATE_IDLE;
			}
		}
		break;

	case ALGORITHM_STATE_DIMMING_UP:
		// handle timer
		if (dimmingCount > 0)
		{
			dimmingCount--;
		} else {
			// change state
			algorithmParameters.state = ALGORITHM_STATE_IDLE;
		}
		break;

	case ALGORITHM_STATE_DELTA_TOO_SMALL:
		// nothing to do, since we cannot operate
		(*DH_ReadyCallback)(DAYLIGHT_HARVESTING_ALGORITHM_NO_CHANGE, 0, 0, 0);
		return;

	// default state, mainly error
	default:
		break;
	}

	// 4. call default callback with no changes
	if (DH_ReadyCallback != NULL)
	{
		(*DH_ReadyCallback)(DAYLIGHT_HARVESTING_ALGORITHM_NO_CHANGE, 0, 0, 0);
	}

}

/**
 * Callback of timed out message from sensor.
 *
 * TODO: decide what to do with the algorithm if the communication with the sensor fails.
 */
void sensorReadingTimedOutCallback(void)
{
	sensorCommunicationErrorCallback();
}

/**
 * General error callback for communication with sensor
 *
 * TODO: decide how to handle errors in the communication with the sensor
 */
void sensorCommunicationErrorCallback(void)
{
	// for now, report no change and proceed
	if (DH_ReadyCallback != NULL)
	{
		(*DH_ReadyCallback)(DAYLIGHT_HARVESTING_ALGORITHM_NO_CHANGE, 0xFF, 0xFF, 0);
	}
}

//
//	Auto range function block
//
// Explication:
// The logic can call two functions
// - DHAlgorithm_MeasureMinimal() and
// - DHAlgorithm_MeasureMaximal()
// That will cause the daylight harvesting logic to measure the currently read level at the daylight sensor and assume it is the
// minimal/maximal reading.
// IMPORTANT: the logic must calls these two functions in the same light situation and with as little time difference as possible.
// The daylight harvesting logic assumes that the delta is the level it can influence the illumination by. It also assumes that the
// luminaire provides 500lux at the target and therefore that the delta in the readings is equivalent to this.
// If the delta is too small, the algorithm will not operate and enter the error state
//

/**
 * Callback for errors when calibrating range of algorithm.
 *
 * This includes timeout and invalid answer from sensor. Go into quiet error. The algorithm will be disabled.
 */
void sensorReadingCalibrationErrorCallback(void)
{
	algorithmParameters.state = ALGORITHM_STATE_ERROR;
	(*DH_measurementCompleteCallback)();
}

/**
 * Callback for measured maximal level
 *
 * qparam m Message received as an answer
 */
void sensorReadingCalibrationMaxCalback(ILBMessageStruct* m)
{
	// check if we received a valid answer
	if (m->payloadSize != 2)
	{
		// trigger error callback
		sensorReadingCalibrationErrorCallback();
		return;
	}
	// store measurement
	uint16_t reading = interpretSensorReading((m->payload[0] << 8) | m->payload[1]);
	if (reading > DH_SENSOR_DARK_COUNTS)
	{
		algorithmParameters.maxLevelMeasured = reading - DH_SENSOR_DARK_COUNTS;
	}
	else
	{
		algorithmParameters.maxLevelMeasured = 0;
	}
	// update parameters
	updateAlgorithmParameters();
	// Call the callback
	(*DH_measurementCompleteCallback)();
}

/**
 * Callback for measured minimal level
 *
 * @param m Message received as an answer
 */
void sensorReadingCalibrationMinCalback(ILBMessageStruct* m)
{
	// check if we received a valid answer
	if (m->payloadSize != 2)
	{
		// trigger error callback
		sensorReadingCalibrationErrorCallback();
		return;
	}
	// store measurement
	uint16_t reading = interpretSensorReading((m->payload[0] << 8) | m->payload[1]);
	if (reading > DH_SENSOR_DARK_COUNTS){
		algorithmParameters.minLevelMeasured = reading - DH_SENSOR_DARK_COUNTS;
	}
	else
	{
		algorithmParameters.minLevelMeasured = 0;
	}
	// update parameters
	updateAlgorithmParameters();
	// trigger callback
	(*DH_measurementCompleteCallback)();
}

/**
 * Interface function to trigger measurement of the minimal level
 *
 * This routine must be called in the same light conditions as the DHAlgorithm_MeasureMaximal counterpart
 *
 * @param sensorAddress 	Address of the sensor to probe
 * @param completeCallback 	Callback to call when the measurement was taken
 */
void DHAlgorithm_MeasureMinimal(uint8_t sensorAddress, void (*completeCallback)(void))
{
	DH_measurementCompleteCallback = completeCallback;
	uint8_t payload[] = {PERIPHERAL_MOTION_DAYLIGHT_VOLATILE_REGISTER, PERIPHERAL_MOTION_DAYLIGHT_VOLATILE_DAYLIGHT_DIRECT_LSB, 0x02};	// request unfiltered daylight level from sensor

	// build up message
	ILBMessageStruct messageToSend;
	messageToSend.address = sensorAddress;
	messageToSend.messageType = ILB_MESSAGE_READ;
	messageToSend.payloadSize = 3;
	memcpy(&messageToSend.payload[0], &payload[0], 3);

	// send the message
	coreLib_sendMessage(&messageToSend, &sensorReadingCalibrationMinCalback, DH_SENSOR_LEVEL_REQUEST_TIMEOUT_MS,  &sensorReadingCalibrationErrorCallback);
}

/**
 * Interface function to trigger measurement of the maximal level
 *
 * This routine must be called in the same light conditions as the DHAlgorithm_MeasureMinimal counterpart
 *
 * @param sensorAddress 	Address of the sensor to probe
 * @param completeCallback 	Callback to call when the measurement was taken
 */
void DHAlgorithm_MeasureMaximal(uint8_t sensorAddress, void (*completeCallback)(void))
{
	DH_measurementCompleteCallback = completeCallback;
	uint8_t payload[] = {PERIPHERAL_MOTION_DAYLIGHT_VOLATILE_REGISTER, PERIPHERAL_MOTION_DAYLIGHT_VOLATILE_DAYLIGHT_DIRECT_LSB, 0x02};	// request unfiltered daylight level from sensor

	// create messaget to send
	ILBMessageStruct messageToSend;
	messageToSend.address = sensorAddress;
	messageToSend.messageType = ILB_MESSAGE_READ;
	messageToSend.payloadSize = 3;
	memcpy(&messageToSend.payload[0], &payload[0], 3);

	// send message
	coreLib_sendMessage(&messageToSend, &sensorReadingCalibrationMaxCalback, DH_SENSOR_LEVEL_REQUEST_TIMEOUT_MS, &sensorReadingCalibrationErrorCallback);
}

/**
 * Updates the parameters for the algorithm.
 *
 * This routine should be called if the range needs update. It will determine the available delta and set the new target
 */
void updateAlgorithmParameters(void)
{
	// check range
	if (algorithmParameters.maxLevelMeasured <= algorithmParameters.minLevelMeasured)
	{
		algorithmParameters.state = ALGORITHM_STATE_DELTA_TOO_SMALL;
		return;
	}

	// calculate delta
	algorithmParameters.availableDelta = algorithmParameters.maxLevelMeasured - algorithmParameters.minLevelMeasured;
	// check for sufficient range of delta
	if (algorithmParameters.availableDelta < (DH_ALGORITHM_NUMBER_OF_BANDS * DH_SENSOR_TARGET_TOLERANCE_PM))
	{
		// range too small for reliable operation
		algorithmParameters.state = ALGORITHM_STATE_DELTA_TOO_SMALL;
		return;
	}

	// set the target
	// we assume that the measured delta (corresponding to 100% vs. 0% light output) corresponds to 500lux)
	algorithmParameters.targetLevel = algorithmParameters.maxLevelMeasured - algorithmParameters.minLevelMeasured + DH_SENSOR_DARK_COUNTS;
	algorithmParameters.state = ALGORITHM_STATE_IDLE;
}

/**
 * Interpret the values of the sensor register as lux
 *
 * @param registerContent
 * @return
 */
uint16_t interpretSensorReading(uint16_t registerContent)
{
	if ((registerContent & 0x8000) == 0x8000)
	{
		return (registerContent & ~0x8000);
	}
	return registerContent * 10;
}


//
//	Manual calibration
//
void DHAlgorithm_UseCurrentLevelAsTaget(uint8_t sensorAddress, void (*callback)(void))
{
	// store callback
	MeasurementDoneCallback = callback;

	// send request to sensor

	algorithmParameters.state = ALGORITHM_STATE_MANUAL;

	uint8_t payload[] = {0x03, 0x01, 0x02};	// request filtered daylight level from sensor
	ILBMessageStruct messageToSend;
	messageToSend.address = sensorAddress;
	messageToSend.messageType = ILB_MESSAGE_READ;
	messageToSend.payloadSize = 3;
	memcpy(&messageToSend.payload[0], &payload[0], 3);
	coreLib_sendMessage(&messageToSend, &sensorReadingReceivedCallback, DH_SENSOR_LEVEL_REQUEST_TIMEOUT_MS, &sensorReadingTimedOutCallback);
}
