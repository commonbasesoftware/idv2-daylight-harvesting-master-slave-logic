/*
 * daylightHarvesting.h
 *
 *  Created on: 25.08.2021
 *      Author: A.Niggebaum
 */

#ifndef DAYLIGHTHARVESTING_DAYLIGHTHARVESTING_H_
#define DAYLIGHTHARVESTING_DAYLIGHTHARVESTING_H_

typedef enum DaylightHarvestingChangeRequestEnum
{
	DAYLIGHT_HARVESTING_ALGORITHM_NO_CHANGE,
	DAYLIGHT_HARVESTING_ALGORITHM_CHANGE_REQUIRED
} DaylightHarvestingChangeRequestEnumTypeDef;

//
//	Public function prototypes
//
void DHAlgorithm_Configure(uint8_t maxLevel, uint8_t minLevel);
void DHAlgorithm_Reset(void);		// should be called when modes are changed
void DHAlgorithm_Trigger(uint8_t sensorAddress, void (*callback)(DaylightHarvestingChangeRequestEnumTypeDef change, uint8_t directLevel, uint8_t indirectLevel, uint16_t transitionTimeMS));
void DHAlgorithm_SetTargetLevel(uint16_t level);
uint16_t DHAlgorithm_GetTargetLevel(void);
void DHAlgorithm_MeasureMinimal(uint8_t sensorAddress, void (*completeCallback)(void));
void DHAlgorithm_MeasureMaximal(uint8_t sensorAddress, void (*completeCallback)(void));
void DHAlgorithm_UseCurrentLevelAsTaget(uint8_t sensorAddress, void (*completeCallback)(void));

#endif /* DAYLIGHTHARVESTING_DAYLIGHTHARVESTING_H_ */
