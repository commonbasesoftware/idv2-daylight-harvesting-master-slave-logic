/*
 * modes.h
 *
 *  Created on: 18.10.2021
 *      Author: A.Niggebaum
 */

#ifndef LOGIC_MODES_H_
#define LOGIC_MODES_H_

#include <stdint.h>

#define LIGHT_ENGINE_HOLD_TIME_FOREVER	0x00

typedef enum ActiveDeactiveEnum {
	DEACTIVATED = 0,
	ACTIVATED	= 1
} ActiveDeactive_t;

typedef struct LightEngineOutputStruct {
	uint8_t level;
	uint16_t mired;
	uint16_t fadeTimeMS;
	uint16_t holdTimeS;
} LightEngineOutput_t;

typedef enum LightEngineTypeEnum {
	LIGHT_ENGINE_ATTACHED,
	LIGHT_ENGINE_PERIPHERAL,
	LIGHT_ENGINE_FOLLOWER,
} LightEngineType_t;

typedef struct LightEngineOutputSequenceStruct {
	LightEngineType_t type;
	uint8_t addressOrGroup;
	ActiveDeactive_t daylightHarvestingTarget;			// if set to activate, the daylight harvesting algorithm will control this output
	uint8_t nTotalOutputs;
	uint8_t currentOutput;
	LightEngineOutput_t* listOfOutputs;
} LightEngineOutputSequence_t;

typedef struct LightEngineModeStruct {
	uint8_t colorDef;
	uint8_t nOfLightEngines;
	ActiveDeactive_t motionSensorActive;
	LightEngineOutputSequence_t* outputSequences;
} LightEngineMode_t;

typedef enum LuminaireModesEnum {
	LUMINAIRE_MODE_DEFAULT,
	LUMINAIRE_MODE_OPEN_OFFICE,
	LUMINAIRE_MODE_SINGLE_OFFICE,
	LUMINAIRE_MODE_CORRIDOR,
	LUMINAIRE_MODE_CLASSROOM,
	LUMINAIRE_MODE_ON,
	LUMINAIRE_MODE_MAX,
} LuminaireModes_t;

//
//	Function prototypes
//
void modes_init(void);							//!< initalise the mode mechanism
void modes_loadFromMemory(uint8_t* colorDef, uint8_t* motionActive, uint8_t* automaticDHCalibrationActive);	//!< load stored mode settings from memory
void modes_nextMode(uint8_t* colorDef, uint8_t* motionActive);			//!< notify the modes engine that the next mode should be selected
void modes_resetCurrentMode(void);				//!< notify the modes engine that motion was detected
void modes_stopTimer(void);
void modes_setOutputOfDaylightTargets(uint8_t level, uint16_t mired, uint16_t fadeTimeMS);
void modes_setDHAlgorithmState(uint8_t state);
void modes_setAutomaticDHCalibration(uint8_t newState);

#endif /* LOGIC_MODES_H_ */
